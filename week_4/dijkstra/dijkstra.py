import heapq
from sys import maxsize as INFINITY


def main():
    ints = input().split(" ")
    ints = [int(x) for x in ints]
    num_vertices, num_edges = ints[0], ints[1]

    adjacency_list = get_adjacency_list(num_vertices, num_edges)

    ints = input().split(" ")
    ints = [int(x) for x in ints]
    vertex_1, vertex_2 = ints[0] - 1, ints[1] - 1

    distances = dijkstra(adjacency_list, vertex_1)
    distance = distances[vertex_2]
    if distance != INFINITY:
        print(distance)
    else:
        print(-1)


def get_adjacency_list(num_vertices, num_edges):
    adjacency_list = [[] for _ in range(num_vertices)]

    for _ in range(num_edges):
        ints = input().split(" ")
        ints = [int(x) for x in ints]
        vertex_1, vertex_2 = ints[0] - 1, ints[1] - 1
        weight = ints[2]
        adjacency_list[vertex_1].append((vertex_2, weight))

    return adjacency_list


def dijkstra(adjacency_list, start):
    num_vertices = len(adjacency_list)

    distances = [INFINITY] * num_vertices
    distances[start] = 0

    visited = [False] * num_vertices

    queue = []
    heapq.heappush(queue, (0, start))

    while len(queue) > 0:
        distance, vertex = heapq.heappop(queue)
        if visited[vertex]:
            continue
        visited[vertex] = True
        for neighbor, weight in adjacency_list[vertex]:
            if visited[neighbor]:
                continue
            d = distance + weight
            if distances[neighbor] > d:
                distances[neighbor] = d
                heapq.heappush(queue, (d, neighbor))

    return distances


if __name__ == "__main__":
    main()
