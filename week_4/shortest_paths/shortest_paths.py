from sys import maxsize as INFINITY
from typing import NamedTuple


class Edge(NamedTuple):
    start: int
    end: int
    weight: int


def read_input():
    num_vertices, num_edges = list(map(int, input().split(" ")))
    edge_list = []
    for _ in range(num_edges):
        vertex_1, vertex_2, weight = list(map(int, input().split(" ")))
        edge_list.append(Edge(vertex_1 - 1, vertex_2 - 1, weight))
    source = int(input()) - 1
    return num_vertices, edge_list, source


def bellman_ford(num_vertices, edge_list, source):
    distances = [INFINITY] * num_vertices
    distances[source] = 0

    for _ in range(num_vertices - 1):
        for edge in edge_list:
            if distances[edge.start] == INFINITY:
                continue
            distances[edge.end] = min(
                distances[edge.end], distances[edge.start] + edge.weight
            )

    can_be_relaxed = []
    for edge in edge_list:
        if (
            distances[edge.start] < INFINITY
            and distances[edge.start] + edge.weight < distances[edge.end]
        ):
            can_be_relaxed.append(edge.end)

    return distances, can_be_relaxed


def get_adjacency_list(num_vertices, edge_list):
    adjacency_list = {vertex: [] for vertex in range(num_vertices)}
    for edge in edge_list:
        adjacency_list[edge.start].append(edge.end)
    return adjacency_list


def get_problematic_vertices(adjacency_list, can_be_relaxed):
    problematic = set()

    def depth_first_search(vertex):
        for neighbor in adjacency_list[vertex]:
            if neighbor not in problematic:
                problematic.add(neighbor)
                depth_first_search(neighbor)

    for vertex in can_be_relaxed:
        depth_first_search(vertex)
    return problematic


def print_distances(distances):
    for distance in distances:
        if distance == INFINITY:
            print("*")
        elif distance == -INFINITY:
            print("-")
        else:
            print(distance)


if __name__ == "__main__":
    num_vertices, edge_list, source = read_input()
    distances, can_be_relaxed = bellman_ford(num_vertices, edge_list, source)
    if len(can_be_relaxed) > 0:
        adjacency_list = get_adjacency_list(num_vertices, edge_list)
        problematic = get_problematic_vertices(adjacency_list, can_be_relaxed)
        for vertex in problematic:
            distances[vertex] = -INFINITY
    print_distances(distances)
