from sys import maxsize as INFINITY
from typing import List, NamedTuple, Tuple


def main():
    ints = map(int, input().split(" "))
    num_vertices, num_edges = next(ints), next(ints)
    edge_list, source_vertices = get_edges_source_vertices(num_edges)

    if has_negative_cycle(edge_list, num_vertices, source_vertices):
        print(1)
    else:
        print(0)


# In this problem, the edge list representation will be used
class Edge(NamedTuple):
    start: int
    end: int
    weight: int


EdgeList = List[Edge]


def get_edges_source_vertices(num_edges: int) -> Tuple[EdgeList, List[int]]:
    edge_list: EdgeList = []
    source_vertices: List[int] = []

    for _ in range(num_edges):
        ints = map(int, input().split(" "))
        start, end = next(ints) - 1, next(ints) - 1
        weight = next(ints)

        edge_list.append(Edge(start, end, weight))
        if weight < 0:
            source_vertices.append(start)

    return edge_list, source_vertices


# Bellman-Ford Algorithm
def negative_cycle_bf(edge_list: EdgeList, num_vertices: int, source_vertex: int) -> bool:
    distances = [INFINITY] * num_vertices
    distances[source_vertex] = 0

    # If a negative cycle doesn't exist, num_vertices-1 iterations are enough
    # to correctly compute all distances

    for _ in range(num_vertices):
        no_change = True

        for edge in edge_list:
            if distances[edge.start] == INFINITY:
                continue

            d = distances[edge.start] + edge.weight
            if distances[edge.end] > d:
                distances[edge.end] = d
                no_change = False

        if no_change:
            return False

    return True


def has_negative_cycle(
    edge_list: EdgeList,
    num_vertices: int,
    source_vertices: List[int],
) -> bool:
    idx = 0
    found_cycle = False
    while idx < len(source_vertices) and not found_cycle:
        found_cycle = negative_cycle_bf(edge_list, num_vertices, source_vertices[idx])
        idx += 1
    return found_cycle


if __name__ == "__main__":
    main()
