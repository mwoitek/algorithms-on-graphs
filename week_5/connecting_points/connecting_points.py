from collections import OrderedDict
from math import sqrt
from operator import itemgetter
from typing import Dict
from typing import List
from typing import Tuple

Edge = Tuple[int, int]
Point = Tuple[int, int]


def read_points() -> List[Point]:
    points = []
    num_points = int(input())
    for _ in range(num_points):
        line = input()
        coords = map(int, line.split(" "))
        x, y = next(coords), next(coords)
        points.append((x, y))
    return points


def compute_distance(point_1: Point, point_2: Point) -> float:
    x_1, y_1 = point_1
    x_2, y_2 = point_2
    delta_x = x_1 - x_2
    delta_y = y_1 - y_2
    return sqrt(delta_x**2 + delta_y**2)


def compute_distances(points: List[Point]) -> Dict[Edge, float]:
    distances = {}
    num_points = len(points)
    for i in range(num_points):
        j = i + 1
        while j < num_points:
            distances[(i, j)] = compute_distance(points[i], points[j])
            j += 1
    return OrderedDict((k, v) for k, v in sorted(distances.items(), key=itemgetter(1)))


class DisjointSets:
    def __init__(self, n: int) -> None:
        self.parents: List[int] = [-1] * n
        self.ranks: List[int] = [-1] * n

    def make_set(self, i: int) -> None:
        self.parents[i - 1] = i
        self.ranks[i - 1] = 0

    def find(self, i: int) -> int:
        if i != self.parents[i - 1]:
            self.parents[i - 1] = self.find(self.parents[i - 1])
        return self.parents[i - 1]

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        if self.ranks[i_id - 1] > self.ranks[j_id - 1]:
            self.parents[j_id - 1] = i_id
        else:
            self.parents[i_id - 1] = j_id
            if self.ranks[i_id - 1] == self.ranks[j_id - 1]:
                self.ranks[j_id - 1] += 1


def kruskal(num_points: int, distances: Dict[Edge, float]) -> List[Edge]:
    min_span_tree = []
    ds = DisjointSets(num_points)
    for u in range(num_points):
        ds.make_set(u)
    for u, v in distances:
        if ds.find(u) != ds.find(v):
            min_span_tree.append((u, v))
            ds.union(u, v)
    return min_span_tree


def compute_minimum_distance(
    min_span_tree: List[Edge], distances: Dict[Edge, float]
) -> float:
    return sum(map(lambda e: distances[e], min_span_tree))


if __name__ == "__main__":
    points = read_points()
    distances = compute_distances(points)
    min_span_tree = kruskal(len(points), distances)
    min_dist = compute_minimum_distance(min_span_tree, distances)
    print(f"{min_dist:.10f}")
