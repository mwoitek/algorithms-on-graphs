from math import sqrt
from typing import Dict
from typing import List
from typing import Tuple


class Point:
    def __init__(self, x: int, y: int, id: int) -> None:
        self.x = x
        self.y = y
        self.id = id

    def __str__(self) -> str:
        return f"({self.x}, {self.y})"

    def distance(self, other: "Point") -> float:
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        return sqrt(delta_x**2 + delta_y**2)


def read_input() -> Tuple[List[Point], int]:
    num_points = int(input())
    points = []
    for id in range(num_points):
        x, y = tuple(map(int, input().split(" ")))
        points.append(Point(x, y, id))
    num_clusters = int(input())
    return points, num_clusters


class Edge:
    def __init__(self, node_1: Point, node_2: Point) -> None:
        self.node_1 = node_1
        self.node_2 = node_2
        self.weight = node_1.distance(node_2)


class DisjointSets:
    def __init__(self, size: int) -> None:
        self.num_components = 0
        self.parents = [-1] * size
        self.ranks = [-1] * size

    def make_set(self, i: int) -> None:
        self.num_components += 1
        self.parents[i - 1] = i
        self.ranks[i - 1] = 0

    def find(self, i: int) -> int:
        if i != self.parents[i - 1]:
            self.parents[i - 1] = self.find(self.parents[i - 1])
        return self.parents[i - 1]

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        self.num_components -= 1
        if self.ranks[i_id - 1] > self.ranks[j_id - 1]:
            self.parents[j_id - 1] = i_id
        else:
            self.parents[i_id - 1] = j_id
            if self.ranks[i_id - 1] == self.ranks[j_id - 1]:
                self.ranks[j_id - 1] += 1


class Graph:
    def __init__(self, nodes: List[Point]) -> None:
        self.nodes = nodes
        self.num_nodes = len(nodes)
        self.edge_list = self._build_edge_list()

    def _build_edge_list(self) -> List[Edge]:
        edge_list = []
        for i in range(self.num_nodes):
            j = i + 1
            while j < self.num_nodes:
                edge_list.append(Edge(self.nodes[i], self.nodes[j]))
                j += 1
        return sorted(edge_list, key=lambda e: e.weight)

    def _kruskal(self, num_clusters: int) -> Tuple[DisjointSets, Edge]:
        sets = DisjointSets(self.num_nodes)
        for node_id in range(self.num_nodes):
            sets.make_set(node_id)
        i = 0
        while sets.num_components > num_clusters:
            edge = self.edge_list[i]
            node_id_1 = edge.node_1.id
            node_id_2 = edge.node_2.id
            if sets.find(node_id_1) != sets.find(node_id_2):
                sets.union(node_id_1, node_id_2)
            i += 1
        while i < len(self.edge_list):
            edge = self.edge_list[i]
            node_id_1 = edge.node_1.id
            node_id_2 = edge.node_2.id
            if sets.find(node_id_1) != sets.find(node_id_2):
                break
            i += 1
        return sets, self.edge_list[i]

    # To solve this problem, this method is not necessary. However, it is useful for
    # checking that the clusters produced by this algorithm make sense.
    def get_clusters(self, num_clusters: int) -> Dict[str, int]:
        clusters_dict = {}
        sets, _ = self._kruskal(num_clusters)
        cluster_ids = {}
        i = 0
        for id in range(self.num_nodes):
            key = str(self.nodes[id])
            val = sets.find(id)
            if val not in cluster_ids:
                cluster_ids[val] = i
                i += 1
            val = cluster_ids[val]
            clusters_dict[key] = val
        return clusters_dict

    def min_cluster_distance(self, num_clusters: int) -> float:
        _, edge = self._kruskal(num_clusters)
        return edge.weight


if __name__ == "__main__":
    points, num_clusters = read_input()
    g = Graph(points)
    del points
    # Uncomment to print clusters
    # clusters_dict = g.get_clusters(num_clusters)
    # for p, c in clusters_dict.items():
    #     print(f"Point: {p} -> Cluster: {c}")
    d = g.min_cluster_distance(num_clusters)
    print(f"{d:.12f}")
