package main

import (
	"container/list"
	"errors"
	"fmt"
)

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjacencyList := getAdjacencyList(numVertices, numEdges)

	if isBipartite(adjacencyList) {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}

// Queue implementation based on a doubly linked list
type queue struct {
	lst *list.List
}

func newQueue() *queue {
	return &queue{lst: list.New()}
}

func (q *queue) enqueue(value int) {
	q.lst.PushBack(value)
}

func (q *queue) dequeue() error {
	if q.lst.Len() == 0 {
		return errors.New("queue is empty")
	}
	elem := q.lst.Front()
	q.lst.Remove(elem)
	return nil
}

func (q *queue) front() (int, error) {
	if q.lst.Len() == 0 {
		return -1, errors.New("queue is empty")
	}
	value, ok := q.lst.Front().Value.(int)
	if !ok {
		return -1, errors.New("incorrect datatype")
	}
	return value, nil
}

func (q *queue) empty() bool {
	return q.lst.Len() == 0
}

// Solution to the problem

// Gets the adjacency list for undirected graphs
func getAdjacencyList(numVertices, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for vertex := range adjacencyList {
		adjacencyList[vertex] = make([]int, 0)
	}

	var vertex1 int
	var vertex2 int
	for i := 0; i < numEdges; i++ {
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		vertex1--
		vertex2--
		adjacencyList[vertex1] = append(adjacencyList[vertex1], vertex2)
		adjacencyList[vertex2] = append(adjacencyList[vertex2], vertex1)
	}

	return adjacencyList
}

type color int

const (
	white color = iota
	blue
	red
)

func switchBlueRed(c color) color {
	if c == blue {
		return red
	}
	return blue
}

func isBipartite(adjacencyList [][]int) bool {
	numVertices := len(adjacencyList)
	colors := make([]color, numVertices)
	q := newQueue()

	for sourceVertex := 0; sourceVertex < numVertices; sourceVertex++ {
		if colors[sourceVertex] != white {
			continue
		}

		colors[sourceVertex] = blue
		q.enqueue(sourceVertex)

		for !q.empty() {
			vertex, _ := q.front()
			q.dequeue()
			for _, neighbor := range adjacencyList[vertex] {
				if colors[neighbor] == white {
					colors[neighbor] = switchBlueRed(colors[vertex])
					q.enqueue(neighbor)
				} else if colors[neighbor] == colors[vertex] {
					return false
				}
			}
		}
	}

	return true
}
