package main

import (
	"container/list"
	"errors"
	"fmt"
)

const infinity = int(^uint(0) >> 1) // Largest integer that can be stored in an int variable

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjacencyList := getAdjacencyList(numVertices, numEdges)

	var vertex1 int
	var vertex2 int
	fmt.Scanf("%d %d", &vertex1, &vertex2)
	vertex1--
	vertex2--

	distances := breadthFirstSearch(vertex1, adjacencyList)
	distance := distances[vertex2]
	if distance != infinity {
		fmt.Println(distance)
	} else {
		fmt.Println(-1)
	}
}

// Queue implementation based on a doubly linked list
type queue struct {
	lst *list.List
}

func newQueue() *queue {
	return &queue{lst: list.New()}
}

func (q *queue) enqueue(value int) {
	q.lst.PushBack(value)
}

func (q *queue) dequeue() error {
	if q.lst.Len() == 0 {
		return errors.New("queue is empty")
	}
	elem := q.lst.Front()
	q.lst.Remove(elem)
	return nil
}

func (q *queue) front() (int, error) {
	if q.lst.Len() == 0 {
		return -1, errors.New("queue is empty")
	}
	value, ok := q.lst.Front().Value.(int)
	if !ok {
		return -1, errors.New("incorrect datatype")
	}
	return value, nil
}

func (q *queue) empty() bool {
	return q.lst.Len() == 0
}

// Solution to the problem

// Gets the adjacency list for undirected graphs
func getAdjacencyList(numVertices, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for vertex := range adjacencyList {
		adjacencyList[vertex] = make([]int, 0)
	}

	for i := 0; i < numEdges; i++ {
		var vertex1 int
		var vertex2 int
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		vertex1--
		vertex2--
		adjacencyList[vertex1] = append(adjacencyList[vertex1], vertex2)
		adjacencyList[vertex2] = append(adjacencyList[vertex2], vertex1)
	}

	return adjacencyList
}

func breadthFirstSearch(sourceVertex int, adjacencyList [][]int) []int {
	distances := make([]int, len(adjacencyList))
	for vertex := range distances {
		distances[vertex] = infinity
	}
	distances[sourceVertex] = 0

	q := newQueue()
	q.enqueue(sourceVertex)

	for !q.empty() {
		vertex, _ := q.front()
		q.dequeue()
		for _, neighbor := range adjacencyList[vertex] {
			if distances[neighbor] == infinity {
				q.enqueue(neighbor)
				distances[neighbor] = distances[vertex] + 1
			}
		}
	}

	return distances
}
