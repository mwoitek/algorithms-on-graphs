package main

import (
	"fmt"
)

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjLstOrig, adjLstTrans := getAdjacencyLists(numVertices, numEdges)

	numComponents := numStronglyConnectedComponents(adjLstOrig, adjLstTrans)
	fmt.Println(numComponents)
}

// Returns two adjacency lists: one for the original graph, and another for the
// transpose graph
func getAdjacencyLists(numVertices, numEdges int) ([][]int, [][]int) {
	adjLstOrig := make([][]int, numVertices)
	adjLstTrans := make([][]int, numVertices)
	for i := 0; i < numVertices; i++ {
		adjLstOrig[i] = make([]int, 0)
		adjLstTrans[i] = make([]int, 0)
	}

	var vertex1 int
	var vertex2 int
	for i := 0; i < numEdges; i++ {
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		vertex1--
		vertex2--
		adjLstOrig[vertex1] = append(adjLstOrig[vertex1], vertex2)
		adjLstTrans[vertex2] = append(adjLstTrans[vertex2], vertex1)
	}

	return adjLstOrig, adjLstTrans
}

func getPostOrder(adjacencyList [][]int) []int {
	numVertices := len(adjacencyList)
	postOrder := make([]int, numVertices)
	visited := make([]bool, numVertices)
	idx := 0

	var depthFirstSearch func(vertex int)
	depthFirstSearch = func(vertex int) {
		visited[vertex] = true
		for _, neighbor := range adjacencyList[vertex] {
			if !visited[neighbor] {
				depthFirstSearch(neighbor)
			}
		}
		postOrder[idx] = vertex
		idx++
	}

	for vertex := 0; vertex < numVertices; vertex++ {
		if !visited[vertex] {
			depthFirstSearch(vertex)
		}
	}

	return postOrder
}

func numStronglyConnectedComponents(adjLstOrig, adjLstTrans [][]int) int {
	numComponents := 0
	visited := make([]bool, len(adjLstOrig))

	var depthFirstSearch func(vertex int)
	depthFirstSearch = func(vertex int) {
		visited[vertex] = true
		for _, neighbor := range adjLstTrans[vertex] {
			if !visited[neighbor] {
				depthFirstSearch(neighbor)
			}
		}
	}

	postOrder := getPostOrder(adjLstOrig)
	var vertex int
	for idx := len(postOrder) - 1; idx >= 0; idx-- {
		vertex = postOrder[idx]
		if !visited[vertex] {
			depthFirstSearch(vertex)
			numComponents++
		}
	}

	return numComponents
}
