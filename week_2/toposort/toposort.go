package main

import (
	"fmt"
)

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjacencyList := getAdjacencyList(numVertices, numEdges)

	order := topologicalSort(adjacencyList)
	printSlice(order)
}

func getAdjacencyList(numVertices int, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for i := range adjacencyList {
		adjacencyList[i] = make([]int, 0)
	}

	for i := 0; i < numEdges; i++ {
		var vertex1 int
		var vertex2 int
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		adjacencyList[vertex1-1] = append(adjacencyList[vertex1-1], vertex2-1)
	}

	return adjacencyList
}

// Works only for a directed acyclic graph
func topologicalSort(adjacencyList [][]int) []int {
	numVertices := len(adjacencyList)
	visited := make([]bool, numVertices)
	order := make([]int, numVertices)
	idx := numVertices - 1

	var depthFirstSearch func(vertex int)
	depthFirstSearch = func(vertex int) {
		visited[vertex] = true
		for _, neighbor := range adjacencyList[vertex] {
			if !visited[neighbor] {
				depthFirstSearch(neighbor)
			}
		}
		order[idx] = vertex + 1
		idx--
	}

	for vertex := 0; vertex < numVertices; vertex++ {
		if !visited[vertex] {
			depthFirstSearch(vertex)
		}
	}

	return order
}

func printSlice(intSlice []int) {
	lastIdx := len(intSlice) - 1
	for i := 0; i < lastIdx; i++ {
		fmt.Printf("%d ", intSlice[i])
	}
	fmt.Printf("%d\n", intSlice[lastIdx])
}
