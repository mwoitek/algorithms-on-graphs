package main

import (
	"fmt"
)

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjacencyList := getAdjacencyList(numVertices, numEdges)

	answer := isCyclic(adjacencyList)
	if answer {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}

func getAdjacencyList(numVertices int, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for i := range adjacencyList {
		adjacencyList[i] = make([]int, 0)
	}

	for i := 0; i < numEdges; i++ {
		var vertex1 int
		var vertex2 int
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		adjacencyList[vertex1-1] = append(adjacencyList[vertex1-1], vertex2-1)
	}

	return adjacencyList
}

type vertexLabel int

const (
	unvisited vertexLabel = iota
	entered
	exited
)

func isCyclic(adjacencyList [][]int) bool {
	numVertices := len(adjacencyList)
	labels := make([]vertexLabel, numVertices)
	for vertex := 0; vertex < numVertices; vertex++ {
		labels[vertex] = unvisited
	}

	var hasBackEdgeDFS func(vertex int) bool
	hasBackEdgeDFS = func(vertex int) bool {
		labels[vertex] = entered
		for _, neighbor := range adjacencyList[vertex] {
			if labels[neighbor] == entered || (labels[neighbor] == unvisited && hasBackEdgeDFS(neighbor)) {
				return true
			}
		}
		labels[vertex] = exited
		return false
	}

	vertex := 0
	foundCycle := false
	for vertex < numVertices && !foundCycle {
		foundCycle = hasBackEdgeDFS(vertex)
		vertex++
	}
	return foundCycle
}
