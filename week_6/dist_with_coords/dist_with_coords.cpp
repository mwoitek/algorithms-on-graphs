#include <cmath>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_map>
#include <vector>

using std::cin;
using std::vector;

const unsigned INF = std::numeric_limits<unsigned>::max();

// Node position

struct NodePosition {
  int x;
  int y;
};

// Graph

using Node = unsigned;

template <typename WeightType>
struct NodeWeightPair {
  Node node;
  WeightType weight;
  NodeWeightPair(const Node node, const WeightType weight)
      : node(node), weight(weight){};
};

template <typename WeightType>
class Graph {
 private:
  std::unordered_map<Node, vector<NodeWeightPair<WeightType>>> adjacency_list;
  const unsigned num_nodes;

 public:
  explicit Graph(const unsigned num_nodes) : num_nodes(num_nodes) {}

  void add_edge(const Node node_1, const Node node_2, const WeightType weight) {
    adjacency_list[node_1].emplace_back(node_2, weight);
  }

  auto has_start_node(const Node node) const -> bool {
    return adjacency_list.find(node) != adjacency_list.cend();
  }

  auto neighbors(const Node node) const
      -> const vector<NodeWeightPair<WeightType>>& {
    return adjacency_list.at(node);
  }

  auto number_of_nodes() const -> unsigned { return num_nodes; }
};

// Reading input

struct NodePair {
  Node node_1;
  Node node_2;
};

struct Input {
  vector<NodePosition> node_positions;
  Graph<unsigned> graph;
  vector<NodePair> query_nodes;
};

auto read_input() -> Input {
  unsigned num_nodes = 0U;
  unsigned num_edges = 0U;
  cin >> num_nodes >> num_edges;

  vector<NodePosition> node_positions(num_nodes);
  for (unsigned i = 0U; i < num_nodes; ++i) {
    int x = 0;  // NOLINT
    int y = 0;  // NOLINT
    cin >> x >> y;
    node_positions[i] = {x, y};
  }

  Node node_1 = 0U;
  Node node_2 = 0U;

  Graph<unsigned> graph(num_nodes);
  for (unsigned i = 0U; i < num_edges; ++i) {
    unsigned weight = 0U;
    cin >> node_1 >> node_2 >> weight;
    graph.add_edge(--node_1, --node_2, weight);
  }

  unsigned num_queries = 0U;
  cin >> num_queries;
  vector<NodePair> query_nodes(num_queries);
  for (unsigned i = 0U; i < num_queries; ++i) {
    cin >> node_1 >> node_2;
    query_nodes[i] = {--node_1, --node_2};
  }

  return {node_positions, graph, query_nodes};
}

// Min-priority queue

template <typename WeightType>
struct GreaterWeight {
  auto operator()(const NodeWeightPair<WeightType>& lhs,
                  const NodeWeightPair<WeightType>& rhs) const -> bool {
    return lhs.weight > rhs.weight;
  }
};

template <typename WeightType>
using NodePQueue = std::priority_queue<NodeWeightPair<WeightType>,
                                       vector<NodeWeightPair<WeightType>>,
                                       GreaterWeight<WeightType>>;

template <typename WeightType>
auto pop_node(NodePQueue<WeightType>& node_queue) -> Node {
  const auto node_weight = node_queue.top();
  node_queue.pop();
  return node_weight.node;
}

// A* implementation (Simple version)

auto euclidean_distance(const NodePosition& pos_1, const NodePosition& pos_2)
    -> double {
  const int delta_x = pos_1.x - pos_2.x;
  const int delta_y = pos_1.y - pos_2.y;
  return std::sqrt(std::pow(delta_x, 2) + std::pow(delta_y, 2));
}

auto a_star(const Graph<unsigned>& graph, const NodePair& node_pair,
            const vector<NodePosition>& node_positions) -> unsigned {
  const Node source = node_pair.node_1;
  const Node target = node_pair.node_2;
  const NodePosition& target_position = node_positions[target];

  const unsigned num_nodes = graph.number_of_nodes();
  vector<unsigned> distances(num_nodes, INF);
  distances[source] = 0U;

  vector<bool> processed(num_nodes);

  NodePQueue<double> node_queue;
  node_queue.emplace(source, 0.0);

  while (!node_queue.empty()) {
    const Node node = pop_node(node_queue);

    if (node == target) {
      return distances[target];
    }

    if (processed[node]) {
      continue;
    }
    processed[node] = true;

    if (!graph.has_start_node(node)) {
      continue;
    }

    for (const auto& node_weight : graph.neighbors(node)) {
      const Node neighbor = node_weight.node;

      if (processed[neighbor]) {
        continue;
      }

      const unsigned alt_distance = distances[node] + node_weight.weight;
      if (alt_distance < distances[neighbor]) {
        distances[neighbor] = alt_distance;
        const double potential =
            euclidean_distance(node_positions[neighbor], target_position);
        node_queue.emplace(neighbor, alt_distance + potential);
      }
    }
  }

  return INF;
}

void run_queries(const Graph<unsigned>& graph,
                 const vector<NodePair>& query_nodes,
                 const vector<NodePosition>& node_positions) {
  for (const NodePair& node_pair : query_nodes) {
    const unsigned distance = a_star(graph, node_pair, node_positions);
    if (distance < INF) {
      std::cout << distance << '\n';
    } else {
      std::cout << -1 << '\n';
    }
  }
}

auto main() -> int {
  std::ios_base::sync_with_stdio(false);
  const Input input = read_input();
  run_queries(input.graph, input.query_nodes, input.node_positions);
  return 0;
}
