#include <iostream>
#include <limits>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using std::unordered_set;
using std::vector;

const unsigned INFINITY = std::numeric_limits<unsigned>::max();

using Node = unsigned;

struct NodeDistPair {
  Node node = 0U;
  unsigned distance = 0U;

  // These constructors have to be defined. Otherwise, this code won't compile.
  NodeDistPair() = default;

  NodeDistPair(const Node node, const unsigned distance)
      : node(node), distance(distance){};
};

struct GreaterDist {
  auto operator()(const NodeDistPair& lhs, const NodeDistPair& rhs) const
      -> bool {
    return lhs.distance > rhs.distance;
  }
};

using NodePQueue =
    std::priority_queue<NodeDistPair, vector<NodeDistPair>, GreaterDist>;

class Graph {
 private:
  std::unordered_map<Node, vector<NodeDistPair>> adjacency_list;
  const unsigned num_nodes;

 public:
  explicit Graph(const unsigned num_nodes) : num_nodes(num_nodes) {}

  void add_edge(const Node node_1, const Node node_2, const unsigned distance) {
    adjacency_list[node_1].emplace_back(node_2, distance);
  }

  auto has_start_node(const Node node) const -> bool {
    return adjacency_list.find(node) != adjacency_list.cend();
  }

  auto neighbors(const Node node) const -> const vector<NodeDistPair>& {
    return adjacency_list.at(node);
  }

  auto number_of_nodes() const -> unsigned { return num_nodes; }
};

// Reading input

struct GraphPair {
  Graph graph;
  Graph reverse_graph;
};

struct NodePair {
  Node node_1;
  Node node_2;
};

struct Input {
  GraphPair graph_pair;
  vector<NodePair> query_nodes;
};

auto read_input() -> Input {
  unsigned num_nodes = 0U;
  unsigned num_edges = 0U;
  std::cin >> num_nodes >> num_edges;

  Node node_1 = 0U;
  Node node_2 = 0U;

  auto graph = Graph(num_nodes);
  auto reverse_graph = Graph(num_nodes);
  for (unsigned i = 0U; i < num_edges; ++i) {
    unsigned distance = 0U;
    std::cin >> node_1 >> node_2 >> distance;
    graph.add_edge(--node_1, --node_2, distance);
    reverse_graph.add_edge(node_2, node_1, distance);  // NOLINT
  }

  unsigned num_queries = 0U;
  std::cin >> num_queries;
  vector<NodePair> query_nodes(num_queries);
  for (unsigned i = 0U; i < num_queries; ++i) {
    std::cin >> node_1 >> node_2;
    query_nodes[i] = {--node_1, --node_2};
  }

  return {{graph, reverse_graph}, query_nodes};
}

// Bidirectional Dijkstra's algorithm

auto pop_node(NodePQueue& p_queue) -> Node {
  const NodeDistPair node_dist = p_queue.top();
  p_queue.pop();
  return node_dist.node;
}

void relax(const Node node_1, const NodeDistPair& node_dist,
           vector<unsigned>& distances, NodePQueue& p_queue) {
  const Node node_2 = node_dist.node;
  const unsigned alt_distance = distances[node_1] + node_dist.distance;
  if (alt_distance < distances[node_2]) {
    distances[node_2] = alt_distance;
    p_queue.emplace(node_2, alt_distance);
  }
}

void process(const Node node, const Graph& graph,
             unordered_set<Node>& processed, vector<unsigned>& distances,
             NodePQueue& p_queue) {
  processed.insert(node);

  if (!graph.has_start_node(node)) {
    return;
  }

  for (const NodeDistPair& node_dist : graph.neighbors(node)) {
    if (processed.find(node_dist.node) != processed.end()) {
      continue;
    }
    relax(node, node_dist, distances, p_queue);
  }
}

auto shortest_distance(const vector<unsigned>& dist_forward,
                       const vector<unsigned>& dist_backward,
                       const unordered_set<Node>& proc_forward,
                       const unordered_set<Node>& proc_backward) -> unsigned {
  unsigned distance = INFINITY;

  for (const Node node : proc_forward) {
    const unsigned alt_distance = dist_forward[node] + dist_backward[node];
    if (dist_backward[node] < INFINITY && alt_distance < distance) {
      distance = alt_distance;
    }
  }

  for (const Node node : proc_backward) {
    const unsigned alt_distance = dist_forward[node] + dist_backward[node];
    if (dist_forward[node] < INFINITY && alt_distance < distance) {
      distance = alt_distance;
    }
  }

  return distance;
}

auto bidirectional_dijkstra(const GraphPair& graph_pair,
                            const NodePair& node_pair) -> unsigned {
  const Graph& graph = graph_pair.graph;
  const Graph& reverse_graph = graph_pair.reverse_graph;

  const Node source = node_pair.node_1;
  const Node target = node_pair.node_2;

  const unsigned num_nodes = graph.number_of_nodes();
  vector<unsigned> dist_forward(num_nodes, INFINITY);
  vector<unsigned> dist_backward(num_nodes, INFINITY);
  dist_forward[source] = 0U;
  dist_backward[target] = 0U;

  unordered_set<Node> proc_forward;
  unordered_set<Node> proc_backward;

  NodePQueue queue_forward;
  NodePQueue queue_backward;
  queue_forward.emplace(source, 0U);
  queue_backward.emplace(target, 0U);

  while (!queue_forward.empty() && !queue_backward.empty()) {
    const Node node_forward = pop_node(queue_forward);
    process(node_forward, graph, proc_forward, dist_forward, queue_forward);
    if (proc_backward.find(node_forward) != proc_backward.end()) {
      return shortest_distance(dist_forward, dist_backward, proc_forward,
                               proc_backward);
    }

    const Node node_backward = pop_node(queue_backward);
    process(node_backward, reverse_graph, proc_backward, dist_backward,
            queue_backward);
    if (proc_forward.find(node_backward) != proc_forward.end()) {
      return shortest_distance(dist_forward, dist_backward, proc_forward,
                               proc_backward);
    }
  }

  return INFINITY;
}

void run_queries(const GraphPair& graph_pair,
                 const vector<NodePair>& query_nodes) {
  for (const NodePair& node_pair : query_nodes) {
    const unsigned distance = bidirectional_dijkstra(graph_pair, node_pair);
    if (distance < INFINITY) {
      std::cout << distance << '\n';
    } else {
      std::cout << -1 << '\n';
    }
  }
}

auto main() -> int {
  std::ios_base::sync_with_stdio(false);
  const Input input = read_input();
  run_queries(input.graph_pair, input.query_nodes);
  return 0;
}
