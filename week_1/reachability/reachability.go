package main

import (
	"fmt"
)

func main() {
	numVertices, numEdges := getPairInts()
	adjacencyList := getAdjacencyList(numVertices, numEdges)
	vertex1, vertex2 := getPairInts()
	vertex1--
	vertex2--

	answer := pathExists(vertex1, vertex2, adjacencyList)
	if answer {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}

func getPairInts() (int, int) {
	var int1 int
	var int2 int
	fmt.Scanf("%d %d", &int1, &int2)
	return int1, int2
}

func getAdjacencyList(numVertices int, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for i := range adjacencyList {
		adjacencyList[i] = make([]int, 0)
	}

	var vertex1 int
	var vertex2 int
	for i := 0; i < numEdges; i++ {
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		vertex1--
		vertex2--
		adjacencyList[vertex1] = append(adjacencyList[vertex1], vertex2)
		adjacencyList[vertex2] = append(adjacencyList[vertex2], vertex1)
	}

	return adjacencyList
}

func pathExists(vertex1 int, vertex2 int, adjacencyList [][]int) bool {
	visited := make([]bool, len(adjacencyList))

	var depthFirstSearch func(vertex int)
	depthFirstSearch = func(vertex int) {
		visited[vertex] = true
		for _, neighbor := range adjacencyList[vertex] {
			if !visited[neighbor] {
				depthFirstSearch(neighbor)
			}
		}
	}

	depthFirstSearch(vertex1)
	return visited[vertex2]
}
