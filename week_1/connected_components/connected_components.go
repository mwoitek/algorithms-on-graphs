package main

import (
	"fmt"
)

func main() {
	var numVertices int
	var numEdges int
	fmt.Scanf("%d %d", &numVertices, &numEdges)
	adjacencyList := getAdjacencyList(numVertices, numEdges)

	numConnectedComponents := computeNumConnectedComponents(adjacencyList)
	fmt.Println(numConnectedComponents)
}

func getAdjacencyList(numVertices int, numEdges int) [][]int {
	adjacencyList := make([][]int, numVertices)
	for i := range adjacencyList {
		adjacencyList[i] = make([]int, 0)
	}

	for i := 0; i < numEdges; i++ {
		var vertex1 int
		var vertex2 int
		fmt.Scanf("%d %d", &vertex1, &vertex2)
		vertex1--
		vertex2--
		adjacencyList[vertex1] = append(adjacencyList[vertex1], vertex2)
		adjacencyList[vertex2] = append(adjacencyList[vertex2], vertex1)
	}

	return adjacencyList
}

func computeNumConnectedComponents(adjacencyList [][]int) int {
	numVertices := len(adjacencyList)
	visited := make([]bool, numVertices)

	var depthFirstSearch func(vertex int)
	depthFirstSearch = func(vertex int) {
		visited[vertex] = true
		for _, neighbor := range adjacencyList[vertex] {
			if !visited[neighbor] {
				depthFirstSearch(neighbor)
			}
		}
	}

	numConnectedComponents := 0
	for vertex := 0; vertex < numVertices; vertex++ {
		if !visited[vertex] {
			depthFirstSearch(vertex)
			numConnectedComponents++
		}
	}
	return numConnectedComponents
}
